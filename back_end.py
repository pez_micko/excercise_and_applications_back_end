#A function that adds 30 days to the input date


def thirty_days_calculator(date):
  try:
    month = int(date[0:2])
    day = int(date[3:5])
    year = int(date[6:])
    if (month > 0 and month < 13) and (day > 0 and day <32) and (year >= 0 and year < 10000):
      if month == 3 or month == 5 or month == 7 or month == 8 or month == 10:
        if day == 1:
            day += 30
        else:
            day -= 1
            month += 1
      elif month == 4 or month == 6 or month == 9 or month == 11:
          month += 1
      elif month == 12:
          year += 1
          month -= 11
          day -= 1
      else:
        if year % 4 == 0:
          if month == 2: #Feb leap
              day += 1
              month += 1
          else: #Jan leap
            if day == 1:
                day += 30
            elif day == 31:
                day -= 30
                month += 2
            else:
                day -= 1
                month += 1
        else:
          if month == 2: #Feb non leap
              day += 2
              month += 1
          else: #Jan non leap
            if day == 1:
                day += 30
            elif day < 30:
                day -= 1
                month += 1
            else:
                day -= 29
                month += 2
      new_date = "%02d/%02d/%04d"
      return new_date % (month, day, year)
    else:
      return "Invalid date"
  except:
    return "Invalid date entry"



#A function that accepts an amount and tax rate and calculates the base price

def base_price_calculator(amount, tax):
  try:
    if amount > 0 and tax >= 0:
      base = amount / ((tax / 100) + 1) #derived formula for base price computation
      base = round(base, 2)
      return base
      #print("The base price is " + str(base))
    else:
      return "Sorry, no below zero value!"
  except:
    return "Invalid entry."