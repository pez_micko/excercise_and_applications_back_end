import back_end

def test_calendar():
    assert back_end.thirty_days_calculator("01/01/2019") == "01/31/2019"
    assert back_end.thirty_days_calculator("01/15/2019") == "02/14/2019"
    assert back_end.thirty_days_calculator("02/01/2019") == "03/03/2019"
    assert back_end.thirty_days_calculator("02/15/2019") == "03/17/2019"
    assert back_end.thirty_days_calculator("03/03/2018") == "04/02/2018"
    assert back_end.thirty_days_calculator("04/30/2016") == "05/30/2016"
    assert back_end.thirty_days_calculator("05/31/1967") == "06/30/1967"
    assert back_end.thirty_days_calculator("06/13/1969") == "07/13/1969"
    assert back_end.thirty_days_calculator("07/17/1985") == "08/16/1985"
    assert back_end.thirty_days_calculator("08/08/0000") == "09/07/0000"
    assert back_end.thirty_days_calculator("09/18/0201") == "10/18/0201"
    assert back_end.thirty_days_calculator("10/24/2700") == "11/23/2700"
    assert back_end.thirty_days_calculator("11/11/1111") == "12/11/1111"
    assert back_end.thirty_days_calculator("12/31/2019") == "01/30/2020"
    assert back_end.thirty_days_calculator("01/01/2020") == "01/31/2020"
    assert back_end.thirty_days_calculator("01/30/2020") == "02/29/2020"
    assert back_end.thirty_days_calculator("02/05/2020") == "03/06/2020"
    assert back_end.thirty_days_calculator("01/31/2020") == "03/01/2020"
    assert back_end.thirty_days_calculator("02/29/2020") == "03/30/2020"


def test_base():
    assert back_end.base_price_calculator(100, 15) == 86.96
    assert back_end.base_price_calculator(263, 10) == 239.09
    assert back_end.base_price_calculator(2.69, 12.5) == 2.39
    assert back_end.base_price_calculator(10500, 20.25) == 8731.81
    assert back_end.base_price_calculator(11, 0) == 11.00

    amount_zero = back_end.base_price_calculator(0, 5)
    amount_negint =  back_end.base_price_calculator(-100, 14)
    tax_negint =  back_end.base_price_calculator(2135, -50)
    tax_negfloat =  back_end.base_price_calculator(657.34, -11.23)
    both_negint = back_end.base_price_calculator(-875.25, -21.12)
    one_negint = back_end.base_price_calculator(-56.21, 89.76)
    stirng_int = back_end.base_price_calculator('asda.dsdas', 15)
    both_string = back_end.base_price_calculator('asd', 'dasdas')

    assert type(amount_zero) is str
    assert type(amount_negint) is str
    assert type(tax_negint) is str
    assert type(tax_negfloat) is str
    assert type(both_negint) is str
    assert type(one_negint) is str
    assert type(stirng_int) is str
    assert type(both_string) is str
